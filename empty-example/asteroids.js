function Asteroid(pos){
    if (pos) {
        this.pos= pos.copy();
    }else{
        this.pos = createVector(random(width), random(height));
    }
    this.r = random(5,65);
    this.vel = p5.Vector.random2D(5,20);
    this.total = floor(random(5,20));
    this.offset = [];
    for (var i = 0; i < this.total; i++) {
        this.offset[i] = random(-15,25);
    }


    this.update = function(){
        this.pos.add(this.vel);
    }

    this.breakup = function () {
        var newAsteroid = [];
        newAsteroid[0] = new Asteroid(this.pos);
        newAsteroid[1] = new Asteroid(this.pos);
        return newAsteroid;
    }


    this.render = function() {
        //push and pop para que no guarden configuraciones con otro
        push();
        stroke(255);
        noFill();
        translate(this.pos.x, this.pos.y);
        //ellipse(0, 0, this.r *2);
        beginShape();
        //para darles formas irregulares solo basta con generar puntos y deformar sus angulos
        for (let index = 0; index < this.total; index++) {
            var angle = map(index,0,this.total,0, TWO_PI);
            var r = this.r + this.offset[index];
            var x = r*cos(angle);
            var y = r*sin(angle);    
            vertex(x, y)        
        }
        endShape(CLOSE);
        pop();
    }

    this.edges = function() {
        if (this.pos.x > width + this.r) {
            this.pos.x = -this.r;
        }else if(this.pos.x < -this.r){
            this.pos.x = width+ this.r;
        }
        if (this.pos.y > width + this.r) {
            this.pos.y = -this.r;
        }else if(this.pos.y < -this.r){
            this.pos.y = height + this.r;
        }
        }
}
