var ship;
//muchos asteroides
var asteroids = [];
var guns = [];

function setup() {
  // put setup code here
  createCanvas(windowWidth, windowHeight);
  ship = new ship();
  for (let index = 0; index < 10; index++) {
    asteroids.push(new Asteroid());
  }
}

function draw() {
  background(0);

  
  asteroids.forEach(element => {
    element.render();
    element.update();
    element.edges();
  });

  guns.forEach(element => {
    element.render();
    element.update();
    for (var j = asteroids.length-1; j>=0;j--) {
      if (element.hits(asteroids[j])) {
        var newAsteroids = asteroids[j].breakup();
        //asteroids.push(newAsteroids);
        asteroids.splice(j,1);
      }
      
    }
      

  });
  //Presiono una tecla manda el grado de rotacion a la funcion setRotation esta genera la rotacion igual al angulo y en turn suma su angulo inicial
  //mas el que recibio
  //Ejemplo para rotar
  //ship.turn(0.1);
  // put drawing code here
  ship.render();
  ship.turn();
  ship.update();
  ship.edges();
}

function keyReleased(){
  ship.setRotation(0);
  if (keyCode == UP_ARROW) {
    ship.boosting(false);
  }
}

function keyPressed(){
  if (key == ' ') {
    guns.push(new gun(ship.pos, ship.heading));
  }
  if (keyCode == RIGHT_ARROW) {
    ship.setRotation(0.1);
  }
  if(keyCode == LEFT_ARROW){
    ship.setRotation(-0.1);
  }
  if (keyCode == UP_ARROW) {
    ship.boosting(true);
  }
  if (keyCode == UP_ARROW) {
    ship.boosting(true);
  }
}



function ship(){
  this.pos = createVector(width/2, height/2);
  this.r = 30;
  //angulo heading PI x 2 igual a angulo de 0
  this.heading = PI * 2;
  //para hacer que rote mejor apenas suelte la tecla
  this.rotation = 0;

  this.isBoosting = false;

  this.boosting = function(b){
    this.isBoosting = b;
  }

  //Para que no se vaya
  this.edges = function() {
    if (this.pos.x > width + this.r) {
      this.pos.x = -this.r;
    }else if(this.pos.x < -this.r){
      this.pos.x = width+ this.r;
    }
    if (this.pos.y > width + this.r) {
      this.pos.y = -this.r;
    }else if(this.pos.y < -this.r){
      this.pos.y = height + this.r;
    }
  }

  //velocidad
  this.vel = createVector(0,0);

  this.update = function(){
    if (this.isBoosting) {
      this.boost();
    }
    this.pos.add(this.vel);
    this.vel.mult(0.95);
  }

  this.boost = function(){
    //Vector que apunta el angulo
    var force = p5.Vector.fromAngle(this.heading);
    force.mult(.5);
    this.vel.add(force);
  }

  this.render = function() {
    push();
    //posicion en la pantalla
    translate(this.pos.x, this.pos.y);
    //rota segun el angulo
    rotate(this.heading + PI / 2);
    //styles
    fill(0);
    stroke(255);

    //P5 make reverse on cartesian plane
    triangle(-this.r, this.r, this.r, this.r, 0, -this.r);
    pop();
  }

  this.setRotation = function(a){
    this.rotation = a;
  }

  //Funcion rotar keyRealeased
  this.turn = function() {
    this.heading += this.rotation;
  }

  //Funcion para rotar segun el angulo que reciba
  /*this.turn = function(angle) {
    this.heading += angle;
  }*/
}